// console.log("Welcome Back");.

// Arithmetic Operators
let x = 1397;
let y = 7831;
let sum  = x + y;
console.log("Result of addition operator: +" + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of product operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);


// Modolus (%) - gets the remainder from 2 divided values
let remainder = y % x;
console.log("Result of modolu operator: " + remainder)

// assignment operator (=)
// basic assignment
let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// shrthand method for assignment operator
assignmentNumber += 2;
console.log("Result of addition assignment operator using a shorthand method: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of subtraction assignment operator using a shorthand method: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator using a shorthand method: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator using a shorthand method: " + assignmentNumber);

// Multiple Operators and Parenthesis

let mdas = 1 + 2 -3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of second pemdas operation: " + pemdas);


// Incrementation versus Decrementation
// incrementation (++)
// decrementation (--)

let z = 1;

// ++z added 1 value to its original value
let increment = ++z;
console.log("Result of pre-incrementation: " + increment);
console.log("Result of pre-incrementation: " + z);

increment = z++;
console.log("Result of post-incrementation: " + increment)
console.log("Result of post-incrementation: " + z);


let decrement = --z;
console.log("Result of pre-decrementation: " + decrement);
console.log("Result of pre-decrementation: " + z);

decrement = z--;
console.log("Result of post-decrementation: " + decrement)
console.log("Result of post-decrementation: " + z);

// Typre Coercion
let numA = "10";
let numB = 12;

let coercion = numA + numB
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// false is equals to 0
let numE = false + 1
console.log(numE);

// true is equals to 1
let numF = true + 1;
console.log(numF);

// COmparison Operators
let juan = "juan";

// Equality operator (==) - checks 2 operands if they have the same content or variables. It may retain boolean value

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log("juan" == "juan");
console.log("juan" == juan);

// inequality operator (!=)
// ! == not

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log("juan" != "juan");
console.log("juan" != juan);
console.log(0 != false);

// Strict equality operator (===) - compares the content and also the data type
console.log(1 === 1);
console.log(1 === 2);
console.log("juan" === juan);


// strict inequality (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log("juan" !== juan);

// relational operator 
let a = 50;
let b = 60;

// GT (>) greater than operator
let isGreaterThan = a > b;
console.log(isGreaterThan);

// LT (<) less than operator
let isLessThan = a < b;
console.log(isLessThan);

// GTE (>==)
let isGTorEqual = a > b;
console.log(isGTorEqual);


// LTE (<==)
let isLTorEqual = a > b;
console.log(isLTorEqual);

let numStr = "30";
console.log(a > numStr);

let str = "twenty";
console.log(b >= str);
// for some occassions, we can receive NaN (Not a Number)


// logical operators
let isLegalAge = true
let isRegistered = false


// logical and operator ( && )
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical and operator: " + allRequirementsMet);

// logical or operator (|| - straight line above enter keyboard)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical or operator: " + someRequirementsMet);

// logical not operator ( ! ) - returns opposite value
let someRequirementsNotMet = !isRegistered
console.log("Result of not logical operator:" + someRequirementsNotMet);
